/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.cuckcoohashing;

/**
 *
 * @author 66955
 */
public class CuckcooHashing {

    static int MAXN = 11;
    static int ver = 2;
    // of MAXN, minimizing wastage
    static int[][] hashtable = new int[ver][MAXN];

// Array to store possible positions for a key
    static int[] pos = new int[ver];

    static void table() {
        for (int j = 0; j < MAXN; j++) {
            for (int i = 0; i < ver; i++) {
                hashtable[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        switch (function) {
            case 1:
                return key % MAXN;
            case 2:
                return (key / MAXN) % MAXN;
        }
        return Integer.MIN_VALUE;
    }

    /* function to place a key in one of its possible positions
* tableID: table in which key has to be placed, also equal
  to function according to which key must be hashed
* cnt: number of times function has already been called
  in order to place the first input key
* n: maximum number of times function can be recursively
  called before stopping and declaring presence of cycle */
    static void place(int key, int tableID, int cnt, int n) {

        if (cnt == n) {
            System.out.printf("%d unpositioned\n", key);
            System.out.printf("Cycle present. REHASH.\n");
            return;
        }
        /* calculate and store possible positions for the key.
    * check if key already present at any of the positions.
    If YES, return. */
        for (int i = 0; i < ver; i++) {
            pos[i] = hash(i + 1, key);
            if (hashtable[i][pos[i]] == key) {
                return;
            }
        }
        /* check if another key is already present at the
       position for the new key in the table
    * If YES: place the new key in its position
    * and place the older key in an alternate position
    for it in the next table */
        if (hashtable[tableID][pos[tableID]] != Integer.MIN_VALUE) {
            int dis = hashtable[tableID][pos[tableID]];
            hashtable[tableID][pos[tableID]] = key;
            place(dis, (tableID + 1) % ver, cnt + 1, n);
        } else {
            hashtable[tableID][pos[tableID]] = key;
        }
    }

    static void printTable() {
        System.out.printf("Final hash tables:\n");

        for (int i = 0; i < ver; i++, System.out.printf("\n")) {
            for (int j = 0; j < MAXN; j++) {
                if (hashtable[i][j] == Integer.MIN_VALUE) {
                    System.out.printf("- ");
                } else {
                    System.out.printf("%d ", hashtable[i][j]);
                }
            }
        }

        System.out.printf("\n");
    }

    /* function for Cuckoo-hashing keys
* keys[]: input array of keys
* n: size of input array */
    static void cuckoo(int keys[], int n) {
        // initialize hash tables to a dummy value
        // (INT-MIN) indicating empty position
        table();

        // start with placing every key at its position in
        // the first hash table according to first hash
        // function
        for (int i = 0, cnt = 0; i < n; i++, cnt = 0) {
            place(keys[i], 0, cnt, n);
        }

        // print the final hash tables
        printTable();
    }
}
