/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.cuckcoohashing;

import static com.pongsakon.cuckcoohashing.CuckcooHashing.cuckoo;

/**
 *
 * @author 66955
 */
public class testCuckcoo {
    public static void main(String[] args) {
        
        int keys_1[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39 };

        int n = keys_1.length;

        cuckoo(keys_1, n);

        int keys_2[] = { 20, 50, 53, 75, 100, 67, 105, 3, 36, 39, 6 };

        int m = keys_2.length;

        cuckoo(keys_2, m);
    }
}
